package com.codeora.bookingservice.model;


import com.codeora.bookingservice.event.BookingEvent;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Booking {
    private String id;
    @NotNull
    private String userId;
    private String vehicleId;
    private Instant creationTime;
    private List<BookingEvent> events = new ArrayList<>();

    public Booking() {

    }
    public Booking(String userId, String vehicleId) {
       this(userId, vehicleId, Instant.now());
    }

    public Booking(String userId, String vehicleId, Instant creationTime) {
        this.userId = userId;
        this.vehicleId = vehicleId;
        this.creationTime = creationTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking that = (Booking) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(vehicleId, that.vehicleId) &&
                Objects.equals(creationTime, that.creationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, vehicleId, creationTime);
    }

    class BookingRequestBuilder {
        private String userId;
        private String vehicleId;
        private Instant creationTime;

        public BookingRequestBuilder builder() {
            return new BookingRequestBuilder();
        }

        public BookingRequestBuilder user(String userId) {
            this.userId = userId;
            return this;
        }

        public BookingRequestBuilder vehicle(String vehicleId) {
            this.vehicleId = vehicleId;
            return this;
        }

        public BookingRequestBuilder creationTime(Instant creationTime) {
            this.creationTime = creationTime;
            return this;
        }

        public Booking build() {
            return new Booking(userId, vehicleId, creationTime == null? Instant.now(): creationTime);
        }

    }
}
