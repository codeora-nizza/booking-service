package com.codeora.bookingservice.event;

import java.time.Instant;

public class BookingEvent {
    private String bookingId;
    private String userId;
    private String vehicleId;
    private Instant timestamp;
    private EventType eventType;

    public enum EventType {
        REQUEST,
        CONFIRMATION,
        CANCELLATION,
        EXPIRATION
    }

    public BookingEvent(String bookingId, String userId, String vehicleId, Instant timestamp, EventType eventType) {
        this.bookingId = bookingId;
        this.userId = userId;
        this.vehicleId = vehicleId;
        this.timestamp = timestamp;
        this.eventType = eventType;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getUserId() {
        return userId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
}
