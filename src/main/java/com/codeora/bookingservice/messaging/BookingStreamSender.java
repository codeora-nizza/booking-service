package com.codeora.bookingservice.messaging;

import com.codeora.bookingservice.event.BookingEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

@Component
public class BookingStreamSender {
    private static final Logger LOG = LoggerFactory.getLogger(BookingStreamSender.class);

    private final BookingStreams bookingStreams;

    @Autowired
    public BookingStreamSender(BookingStreams bookingStreams) {
        this.bookingStreams = bookingStreams;
    }

    public void sendBookingEvent(final BookingEvent event) {
        LOG.info("sending booking request {}", event);
        assert(event.getEventType() != null);

        MessageChannel channel = bookingStreams.publishChannel();
        channel.send(MessageBuilder.withPayload(event)
                .setHeader("type", BookingEvent.EventType.REQUEST.name())
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }

}
