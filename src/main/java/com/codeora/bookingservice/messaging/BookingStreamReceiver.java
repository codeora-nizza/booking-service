package com.codeora.bookingservice.messaging;

import com.codeora.bookingservice.event.BookingEvent;
import com.codeora.bookingservice.service.BookingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class BookingStreamReceiver {
    private static final Logger LOG = LoggerFactory.getLogger(BookingStreamReceiver.class);

    private BookingService bookingService;

    @Autowired
    public BookingStreamReceiver(BookingService bookingService) {
        this.bookingService = bookingService;
    }
    /**
     * since we put request, confirmation, cancellation, expiration events in the same topic,
     * here we must filter the needed events and handle them with corresponding logic
     * @param confirm
     */
    @StreamListener(target = BookingStreams.INPUT, condition = "headers['type'] == 'CONFIRMATION'")
    public void receiveConfirmation(@Payload BookingEvent confirm) {
        LOG.info("received booking confirmation: {}", confirm);
        bookingService.confirmBooking(confirm);
    }

    @StreamListener(target = BookingStreams.INPUT, condition = "headers['type'] =='REQUEST'")
    public void receiveRequest(@Payload BookingEvent request) {
        LOG.info("received booking request {}", request);
    }
}
