package com.codeora.bookingservice.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface BookingStreams {
    String INPUT = "booking-input";
    String OUTPUT = "booking-output";

    /*
     */
    @Input(INPUT)
    SubscribableChannel subscriptionChannel();

    @Output(OUTPUT)
    MessageChannel publishChannel();

}
