package com.codeora.bookingservice.messaging;

import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(BookingStreams.class)
public class BookingStreamConfig {
}
