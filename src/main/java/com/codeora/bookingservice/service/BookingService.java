package com.codeora.bookingservice.service;

import com.codeora.bookingservice.event.BookingEvent;
import com.codeora.bookingservice.messaging.BookingStreamReceiver;
import com.codeora.bookingservice.messaging.BookingStreamSender;
import com.codeora.bookingservice.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class BookingService {
    private BookingStreamSender sender;
    // the service should not depend on stream receiver: it's always the receiver that calls service methods
    // private BookingStreamReceiver receiver;
    @Autowired
    private BookingService(BookingStreamSender sender) {
        this.sender = sender;
    }

    public void createBooking(Booking req) {
        // save booking in Cache or DB
        sender.sendBookingEvent(makeBookingRequest(req));
    }

    public void cancelBooking() {
    }

    public void expireBooking() {
    }

    public void confirmBooking(BookingEvent event) {
    }

    private BookingEvent makeBookingRequest(Booking req) {
        return new BookingEvent("TestBookingID0001",
                req.getUserId(), req.getVehicleId(), Instant.now(), BookingEvent.EventType.REQUEST);
    }
}
