package com.codeora.bookingservice.rest;

import com.codeora.bookingservice.model.Booking;
import com.codeora.bookingservice.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.Instant;

/**
 * Endpoint for incoming booking requests from user client
 */
@RestController("/request")
public class BookingController {
    private BookingService bookingService;
    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    // TODO: be HATEOAS
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Booking createBooking(@RequestBody @Valid Booking request) {
        // TODO: websocket? asynchronous response?
        request.setCreationTime(Instant.now());
        bookingService.createBooking(request);
        request.setId("B2009012100000001");
        return request;
    }
}
