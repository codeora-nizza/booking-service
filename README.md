booking service is incharge of *persisting and propagating* booking related events 
 
 - checking validity of booking request
    - if user is authenticated
    - if user has not done overlapping bookings
 
 - forwards booking requests to fleet manager
  
 - receives booking confirmations from fleet manager 
 
 - remembers the time when the booking is created, confirmed, canceled
 
 -  
 
 Free Floating booking 
 
 - 